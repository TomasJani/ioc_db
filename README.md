# IOC Database Manager

> Simple CLI application capable of sending data into DB and reading them from it.

## Technologies
 - DB: PostreSQL
 - ORM: SQLAlchemy
 - CLI: click
 - Dependency manager: pipenv

## Running
 - docker-compose build
 - docker-compose up
 - For running in local enviroment:
   - set DB variable in ioc_db/db/db_config.py file.
   - run `sh run.sh` or try your own command. (examples in run.sh)

## Models / Tables
  - Source: holds source url for ip and url
  - Url
  - Ip
  - IpData: holds additional data for Ip model

## Structure / Functionaity
 - Parsers - parse data from given url
 - Cretors - takes parsers data and sends them to DB
 - Readers - queries data from DB 
 - Processor - orchestrates functionaity described above


## Commands:
 - **drop**  Drop all tables on operational DB.
 - **load**  Loads data from url associated with name into DB.
 - **read**  Read data from DB associated with source.

### Load
| Options | Description |
| ------ | ------ |
| -n, --name | Name of provider of given data source. |
| -m, --max_rows INTEGER  | Maximum number of loaded rows. |
| --help | Show this message and exit. |

### Read

| Options | Description |
| ------ | ------ |
| -s, --source | Name of source associated with data. |
| -m, --max_rows INTEGER  | Maximum number of loaded rows. |
| --help | Show this message and exit. |