FROM python:3.6.12-slim-stretch

WORKDIR /usr/src/app

COPY . .

RUN pip3 install -U -q pipenv
RUN pipenv install --dev


CMD [ "sh", "run.sh" ]