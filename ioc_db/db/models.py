import re

from sqlalchemy import Column, Integer, String, Sequence, ForeignKey, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, validates

from db.db import engine
from parser_templates import URL_TEMPLATE, IP_TEMPLATE

Base = declarative_base()


class Source(Base):
    __tablename__ = 'source'
    id = Column(Integer, Sequence('source_id_seq'), primary_key=True)
    ips = relationship("Ip", back_populates="source")
    urls = relationship("Url", back_populates="source")

    url = Column(String(50), unique=True)

    @validates('url')
    def validate_address(self, key, url):
        assert re.match(URL_TEMPLATE, url)
        return url

    def __repr__(self):
        return f"<Source(url='{self.url}')>"


class Url(Base):
    __tablename__ = 'url'
    id = Column(Integer, Sequence('url_id_seq'), primary_key=True)
    source_id = Column(Integer, ForeignKey('source.id'))
    source = relationship("Source", back_populates="urls")

    url = Column(String(2000), unique=True)

    @validates('url')
    def validate_address(self, key, url):
        assert re.match(URL_TEMPLATE, url)
        return url

    def __repr__(self):
        return f"<Url(url='{self.url}')>"


class Ip(Base):
    __tablename__ = 'ip'
    id = Column(Integer, Sequence('ip_id_seq'), primary_key=True)
    source_id = Column(Integer, ForeignKey('source.id'))
    source = relationship("Source", back_populates="ips")
    data = relationship("IpData", uselist=False, back_populates="ip")

    ip = Column(String(45), unique=True)

    @validates('ip')
    def validate_address(self, key, ip):
        assert re.match(IP_TEMPLATE, ip)
        return ip

    def __repr__(self):
        return f"<Ip(ip='{self.ip}')>"


class IpData(Base):
    __tablename__ = 'ipdata'
    id = Column(Integer, Sequence('ipdata_id_seq'), primary_key=True)
    ip_id = Column(Integer, ForeignKey('ip.id'), unique=True)
    ip = relationship("Ip", back_populates="data")

    risk = Column(Integer)
    reliability = Column(Integer)
    activity = Column(String(50))
    country = Column(String(5))
    city = Column(String(100))
    latitude = Column(Float(15))
    longitude = Column(Float(15))
    dont_know = Column(Integer)

    def __repr__(self):
        return f"<Source(risk='{self.risk}' reliability='{self.reliability}' activity='{self.activity}' " \
               f"country='{self.country}' city='{self.city}' latitude='{self.latitude}' longitude='{self.longitude}' " \
               f"dont_know='{self.dont_know}')>"


Base.metadata.create_all(bind=engine)
