DB = "postgresql"
DRIVER = "psycopg2"
USERNAME = "postgres"
PASSWORD = "password"
HOST = "db"  # db for docker, localhost for loacl
PORT = "5432"
DATABASE = "postgres"

CONNECTION_STRING = f"{DB}+{DRIVER}://{USERNAME}:{PASSWORD}@{HOST}:{PORT}/{DATABASE}"
