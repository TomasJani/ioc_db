from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from db.db_config import CONNECTION_STRING

engine = create_engine(CONNECTION_STRING)
Session = sessionmaker(bind=engine)
