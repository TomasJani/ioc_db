import logging
from functools import partial

from sqlalchemy.exc import IntegrityError

from db.db import Session
from db.models import Ip, Url, IpData, Source


def add_to_db(url_data, create_entity):
    session = Session()
    url, data = url_data
    source = get_or_create_source(session, url)
    create_entity_source = partial(create_entity, source)
    objs = map(create_entity_source, data)
    for obj in objs:
        try:
            session.add_all(obj)
            session.commit()
        except IntegrityError:
            logging.warning(f"Object {obj} failed unique constraint.")
            session.rollback()


def create_ip(source, ip):
    return [Ip(ip=ip, source=source)]


def create_url(source, url):
    return [Url(url=url, source=source)]


def create_source(url):
    return Source(url=url)


def create_ip_data(source, ip_str_cols):
    ip_str, cols = ip_str_cols
    ip = Ip(ip=ip_str, source=source)
    ip_data = IpData(**cols, ip=ip)
    return [ip, ip_data]


def get_or_create_source(session, url):
    source = session.query(Source).filter_by(url=url).first()
    if not source:
        source = create_source(url)
        session.add(source)
        session.commit()
    return source
