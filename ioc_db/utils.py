import logging

import requests


def load_data(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return list(response.text.splitlines())
    except Exception as e:
        logging.error(f"Something went wrong when fetching data.\n{e}")

    return []
