import click
import termtables as tt

from creators import create_ip, create_ip_data, create_url, add_to_db
from db.db import Session
from parser_templates import IP_TEMPLATE, IP_DATA_TEMPLATE, URL_TEMPLATE
from parsers import parse_ip_data_line, parse_content
from reader import ip_reader, url_reader, ip_data_reader

db_processors = {
    "badips":
        (
            ("https://www.badips.com/get/list/any/2", IP_TEMPLATE, lambda ip: ip),
            create_ip,
            ip_reader,
        ),
    "alienvault":
        (
            ("http://reputation.alienvault.com/reputation.data", IP_DATA_TEMPLATE, parse_ip_data_line),
            create_ip_data,
            ip_data_reader,
        ),
    "openphish":
        (
            ("https://openphish.com/feed.txt", URL_TEMPLATE, lambda url: url),
            create_url,
            url_reader,
        ),
}


def process_db_processor(name, max_rows):
    click.echo(f"Parsing {name} data into DB.")
    parsed_args, creator, _ = db_processors[name]
    parsed_data = parse_content(*parsed_args, max_rows)
    click.echo(f"Loading {name} data into DB.")
    add_to_db(parsed_data, creator)


def read_data(name, max_rows):
    session = Session()
    _, _, reader = db_processors[name]
    click.echo(f"\nReading data for {name} source.\n")
    header, data = reader(session, max_rows)

    if not data:
        click.echo(f"Table from source {name} is empty.")
        return

    table = tt.to_string(
        data,
        header=header,
        style=tt.styles.ascii_thin_double,
        padding=(0, 2),
    )
    click.echo(table)
