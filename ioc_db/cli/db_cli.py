import click

from cli.processor import db_processors, process_db_processor, read_data
from config import LOAD_MAX_ROWS, SHOW_MAX_ROWS
from db.db import engine
from db.models import Ip, Source, Url, IpData


@click.group()
def cli():
    """IOC Database manager

    Simple CLI application capable of sending data into DB
    and reading them from it."""


@cli.command("load")
@click.option(
    "-n",
    "--name",
    default="all",
    type=click.Choice(["all", *db_processors.keys()], case_sensitive=False),
    help="Name of provider of given data source.",
)
@click.option(
    "-m",
    "--max_rows",
    default=LOAD_MAX_ROWS,
    type=int,
    help="Maximum number of loaded rows.",
)
def load(name, max_rows):
    """Loads data from url associated with name into DB."""
    if name == "all":
        for p_name in db_processors.keys():
            process_db_processor(p_name, max_rows)
    else:
        process_db_processor(name, max_rows)


@cli.command("read")
@click.option(
    "-s",
    "--source",
    default="all",
    type=click.Choice(["all", *db_processors.keys()], case_sensitive=False),
    help="Name of source associated with data.",
)
@click.option(
    "-m",
    "--max_rows",
    default=SHOW_MAX_ROWS,
    type=int,
    help="Maximum number of loaded rows.",
)
def read(source, max_rows):
    """Read data from DB associated with source."""
    if source == "all":
        for p_name in db_processors.keys():
            read_data(p_name, max_rows)
    else:
        read_data(source, max_rows)


@cli.command("drop")
def drop():
    """Drop all tables on operational DB."""
    click.echo("Dropping all tables.")
    IpData.__table__.drop(engine)
    Ip.__table__.drop(engine)
    Url.__table__.drop(engine)
    Source.__table__.drop(engine)
