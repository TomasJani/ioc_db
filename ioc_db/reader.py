from db.models import Ip, Url, IpData


def reader(session, max_rows, header, data_class, row_function):
    data = session.query(data_class).limit(max_rows).all() or []
    table_data = list(map(row_function, data))
    return header, table_data


def ip_reader(session, max_rows):
    header = ["ID", "IP"]
    return reader(session, max_rows, header, Ip, lambda ip: [ip.id, ip.ip])


def url_reader(session, max_rows):
    header = ["ID", "URL"]

    def row_function(url):
        return [url.id, trim(url.url, 50)]

    return reader(session, max_rows, header, Url, row_function)


def ip_data_reader(session, max_rows):
    header = ["ID", "RISK", "RELIABILITY", "ACTIVITY", "COUNTRY", "CITY", "LATITUDE", "LONGITUDE"]

    def row_function(ip_data):
        return [ip_data.id, ip_data.risk, ip_data.reliability, ip_data.activity, ip_data.country,
                ip_data.city, ip_data.latitude, ip_data.longitude]

    return reader(session, max_rows, header, IpData, row_function)


def trim(string, max_chars):
    return string[:(max_chars - 3)] + "..." if (len(string) > max_chars) else string
