import logging
import re

from config import LOAD_MAX_ROWS
from utils import load_data


def parse_content(url, template, parse_line, max_lines=LOAD_MAX_ROWS):
    data = load_data(url)
    parsed_data = []
    current_line = 0
    for line in data:
        if current_line >= max_lines:
            break
        elif re.match(template, line):
            parsed_data.append(parse_line(line))
        else:
            logging.warning(f"Line {line} was not correctly parsed.")
        current_line += 1
    return url, parsed_data


def parse_ip_data_line(line):
    cols = line.split('#', 7)
    [ip, risk, reliability, activity, country, city, latitude_longitude, dont_know] = cols
    [latitude, longitude] = latitude_longitude.split(',')
    return ip, {"risk": risk, "reliability": reliability, "activity": activity, "country": country, "city": city,
                "latitude": latitude, "longitude": longitude, "dont_know": dont_know}
